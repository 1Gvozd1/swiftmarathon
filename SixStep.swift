//Реализовать структуру IOSCollection и создать в ней copy on write по типу

func address(of object: UnsafeRawPointer) -> String {
  let addr = Int(bitPattern: object)
  return String(describing: UnsafeRawPointer(bitPattern: addr))
}

func address(off value: AnyObject) -> String {
  return "\(Unmanaged.passUnretained(value).toOpaque())"
}

struct IOSCollection {
  var name = "Array"
}

class Ref<T> {
  var value: T
  init(value: T) {
    self.value = value
  }
}

struct Container<T> {
  var ref: Ref<T>
  init(value: T) {
    self.ref = Ref(value: value)
  }

  var value: T {
    get {
      return ref.value
    }
    set {
      guard (isKnownUniquelyReferenced(&ref)) else {
        ref = Ref(value: newValue)
        return
      } 
      ref.value = newValue
    }
 }
}


var name = IOSCollection()
var container1 = Container(value: name)
var container2 = container1

print(address(off: container1.ref))
print(address(off: container2.ref))

container2.value.name = "Set"

print(address(off: container1.ref))
print(address(off: container2.ref))

//----------------------------
//Создать протокол *Hotel* с инициализатором, который принимает roomCount, после создать class HotelAlfa добавить свойство roomCount и подписаться на этот протокол
protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa : Hotel {
    var roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

//----------------------------
print("----------------------------")
//Создать protocol GameDice у него {get} свойство numberDice далее нужно расширить Int так, чтобы когда мы напишем такую конструкцию 'let diceCoub = 4 diceCoub.numberDice' в консоле мы увидели такую строку - 'Выпало 4 на кубике'
protocol GameDice {
    var numberDice : Int {get}
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}

let diceCount = 4
let diceValue = diceCount.numberDice

//------------------------------
//Создать протокол с одним методом и 2 свойствами одно из них сделать явно optional, создать класс, подписать на протокол и реализовать только 1 обязательное свойство
protocol Person {
    var name: String { get }
    var age: Int? { get set }
    
    func introduce()
}

class Student : Person {
    var name: String = "Артем"
    var age: Int?
    func introduce() {
        print("Меня зовут \(name)")
    }
}

//------------------------------
print("------------------------------")
//Создать 2 протокола: со свойствами время, количество кода и функцией writeCode(platform: Platform, numberOfSpecialist: Int); и другой с функцией: stopCoding(). Создайте класс: Компания, у которого есть свойства - количество программистов, специализации(ios, android, web)
protocol Coding {
    var time: Double { get set }
    var codeCount: Int { get set }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int)
}

protocol StopCoding {
    func stopCoding()
}

enum Platform {
    case ios
    case android
    case web
   
}

class Company: Coding, StopCoding {
    var numberOfProgrammers: Int
    var specializations : [Platform]
    var time : Double = 0
    var codeCount: Int = 0
    
    init(numberOfProgrammers: Int, specializations:[Platform]) {
        self.numberOfProgrammers = numberOfProgrammers
        self.specializations = specializations
    }
    
    func writeCode(platform: Platform, numberOfSpecialist: Int) {
        if numberOfSpecialist > numberOfProgrammers {
            print("Недостаточно программистов для разработки")
            return
        }
        
        if specializations.contains(platform) {
            time += 1.0
            codeCount += 100
            print("Разработка началась. Пишем код для \(platform)")
        } else {
            print("Компания не пишет код для \(platform)")
        }
        
    }
    
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
    
    
}

let company = Company(numberOfProgrammers: 5, specializations: [.ios, .web])
company.writeCode(platform: .web, numberOfSpecialist: 2)
company.writeCode(platform: .ios, numberOfSpecialist: 6)
company.writeCode(platform: .android, numberOfSpecialist: 3)
company.stopCoding()
