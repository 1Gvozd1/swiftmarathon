//Создать класс родитель и 2 класса наследника
class Animal {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func makeSound() {
        print("Животное издает звук")
    }
}

class Cat: Animal {
    override func makeSound() {
        print("Мяу")
    }
}

class Dog: Animal {
    override func makeSound() {
        print("Гав")
    }
}

//--------------------------------
//Создать класс *House* в нем несколько свойств - *width*, *height* и несколько методов: *create*(выводит площадь),*destroy*(отображает что дом уничтожен)

class House {
  var width: Int
  var height: Int

  init(width: Int, height: Int) {
    self.width = width
    self.height = height
  }

  func create() {
    print("Площадь дома: \(width*height)")
  }

  func destroy() {
    width = 0
    height = 0
    print("Дом уничтожен")
  }
  
}

let myHouse = House(width: 15, height: 30)
myHouse.create()
myHouse.destroy()
myHouse.create()

//--------------------------------
print("------------------------------")
//Создайте класс с методами, которые сортируют массив учеников по разным параметрам
class Student {
    let name: String
    let age: Int
    let group: Int
    let averScore: Double
    
    init(name: String, age: Int, group: Int, averScore: Double) {
        self.name = name
        self.age = age
        self.group = group
        self.averScore = averScore
    }
    
}

class SortStudents {
  func sortName(_ students: [Student]) -> [Student] {
    return students.sorted { $0.name < $1.name } 
  }
  func sortAge(_ students: [Student]) -> [Student] {
    return students.sorted { $0.age < $1.age } 
  }
  func sortGroup(_ students: [Student]) -> [Student] {
    return students.sorted { $0.group < $1.group } 
  }
  func sortAverScore(_ students: [Student]) -> [Student] {
    return students.sorted { $0.averScore < $1.averScore } 
  }
  func printResult(_ students: [Student]) {
    for i in students {
      print("Имя: \(i.name), Возраст: \(i.age), Группа: \(i.group), Средний балл: \(i.averScore)")
    }
  }
}

let sorter = SortStudents()

let student1 = Student(name: "Артем", age: 20, group: 9371, averScore: 4.8 )
let student2 = Student(name: "Александр", age: 19, group: 9371, averScore: 4.7 )
let student3 = Student(name: "Лев", age: 18, group: 6301, averScore: 4.2)
var students = [student1, student2, student3]

print("Начальный массив:")
sorter.printResult(students)

students = sorter.sortName(students)
print("")
print("Сортировка по имени:")
sorter.printResult(students)

students = sorter.sortAge(students)
print("")
print("Сортировка по возрасту:")
sorter.printResult(students)

//--------------------------------
//Написать свою структуру и класс, и пояснить в комментариях - чем отличаются структуры от классов

class StudentClass {
  var name: String = "Артем"
  var age: Int = 21
}
struct StudentStruct {
  var name: String = "Артем"
  var age: Int = 21
}

// Отличия структур и классов
// Экземляры классов хранятся в heap-памяти, а экземпляры структур в stack-памяти. 
// Экземпляры классов передаются по ссылке, в то время как экземпляры структур передаются по значению
// Классы могут наследовать свойства и методы других классов, а структуры не могут.


//--------------------------------
print("------------------------------")
//Напишите простую программу, которая отбирает комбинации в покере из рандомно выбранных 5 карт

struct Card {
   var rank: String
    var suit: String
   
}

let suitArray = ["♥️", "♦️", "♠️", "♣️"]
let rankArray = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"]

var deckOfCards = [Card]()
for i in suitArray {
  for j in rankArray{
    deckOfCards.append(Card(rank:j ,suit: i))
  }
}

func distributionCards() -> [Card] {
  var fiveCards = [Card]()
  print("Наша рука:")
  for _ in 1...5 {
        let randomIndex = Int.random(in: 0..<deckOfCards.count)
        let card = deckOfCards[randomIndex]
        fiveCards.append(card)
        deckOfCards.remove(at: randomIndex)
        print(card.rank, card.suit )
    }
  return fiveCards
}
var fiveCards = distributionCards()

func findCombination(_ fiveCards : [Card]) -> String {
     let sortedFiveCards = fiveCards.sorted { (card1, card2) -> Bool in
        let a  = rankArray.firstIndex(of: card1.rank)!
        let b = rankArray.firstIndex(of: card2.rank)!
        return a < b
    }

    func isRoyalFlush(hand: [Card]) -> Bool {
    let royalRanks = ["10", "J", "Q", "K", "A"]
    let royalSuit = hand[0].suit
    for i in 0..<5 {
      if hand[i].rank != royalRanks[i] || hand[i].suit != royalSuit  {
        return false
      }
    }
  return true
  }
  
  func isFlush(hand: [Card]) -> Bool {
    let flush = hand[0].suit
    for i in 0..<5 {
      if hand[i].suit != flush {
        return false
      }
    }
    return true
  }

  func isStraight(hand: [Card]) -> Bool {
    for i in 0..<4 {
      if rankArray.firstIndex(of:hand[i].rank)! + 1 != rankArray.firstIndex(of:hand[i+1].rank)! {
        return false
      }
    }
    return true
  }

  func isStraightFlush(hand: [Card]) -> Bool {
    if isFlush(hand: hand) && isStraight(hand: hand) {return true}
    return false
  }

   func isFourOfAKind(hand: [Card]) -> Bool {
    var res = 0
    let rank = hand[2].rank
    for i in 0..<5 {
      if(hand[i].rank == rank) {res+=1}
    }
    if res == 4 {return true} else {return false}
    
  }

   func isFullHouse(hand: [Card]) -> Bool {
    var firstRankCount = 0
    var secondRankCount = 0
    for i in 0..<5 {
        if hand[i].rank == hand[hand.count - 1].rank {firstRankCount+=1} else if hand[i].rank == hand[0].rank {secondRankCount+=1}
    }
    if ((firstRankCount == 3 && secondRankCount == 2) || (firstRankCount == 2 && secondRankCount == 3)) {return true} else {return false}
    
  }

   func isThreeOfAKind(hand: [Card]) -> Bool {
  var countRanks = 0 
  let rank = hand[2].rank
  for i in 0..<5 {
      if hand[i].rank == rank {countRanks+=1}
  }
  if countRanks == 3 {return true} else {return false}
    
  }

  func isTwoPairs(hand: [Card]) -> Bool {
    var countFirstRank = 0
    var countSecondRank = 0
    for i in 0..<5 {
        if hand[i].rank == hand[1].rank {countFirstRank += 1} else if hand[i].rank == hand[3].rank {countSecondRank += 1}
    }
    if(countFirstRank == 2 && countSecondRank == 2) {return true} else {return false}
  }

 func isPair(hand: [Card]) -> Bool {
   for i in 0..<4 {
     if(hand[i].rank == hand[i+1].rank){return true} 
   }
   return false
  }
     if isRoyalFlush(hand: sortedFiveCards) {
        return "Вы собрали Роял Флеш. Вау! - Приоритет 1"
    } else if isStraightFlush(hand: sortedFiveCards) {
        return "Вы насобирали на Стрит Флеш - Приоритет 2"
    } else if isFourOfAKind(hand: sortedFiveCards) {
        return "Каре. Большая редкость - Приоритет 3"
    } else if isFullHouse(hand: sortedFiveCards) {
        return "Это Фулл-Хаус - Приоритет 4"
    } else if isFlush(hand: sortedFiveCards) {
        return "Получился Флеш - Приоритет 5"
    } else if isStraight(hand: sortedFiveCards) {
        return "Это стрит, неплохо - Приоритет 6"
    } else if isThreeOfAKind(hand: sortedFiveCards) {
        return "Тройка - Приоритет 7"
    } else if isTwoPairs(hand: sortedFiveCards) {
        return "Две пары - Приоритет 8"
    } else if isPair(hand: sortedFiveCards) {
        return "Пара - Приоритет 9"
    } else {
        return "Увы, это старшая карта"
    }

}
print(findCombination(fiveCards))






