//Создайте по 2 enum с разным типом RawValue
enum Direction : String {
  case Left = "Left!"
  case Right = "Right!"
}

enum Weekday: Int {
    case Monday = 1
    case Tuesday 
    case Wednesday 
    case Thursday
    case Friday
    case Saturday
    case Sunday
}

//--------------------------------
//Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж
enum Gender {
  case Male
  case Female
  case Helicopter
}

enum AgeCategory {
    case Under18
    case From18to65
    case Over65
}

enum Experience {
    case LessThan1Year
    case From1to5Years
    case Over5Years
}

struct Worker {
  var name: String
  var gender: Gender
    var ageCategory: AgeCategory
    var experience: Experience
}

let Artem = Worker(name: "Артем", gender: .Helicopter, ageCategory: .From18to65, experience: .From1to5Years)
print(Artem)

//--------------------------------
//Создать enum со всеми цветами радуги

enum RainbowColor {
  case Red
  case Orange
  case Yellow
  case Green
  case Blue
  case Indigo
  case Violet
}

//--------------------------------
print("--------------------------------")
//Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.
enum FruitColor {
    case Apple(color: String)
    case Banana(color: String)
    case Watermelon(color: String)
    case Peach(color: String)
}

func printFruitColors() {
    let fruits: [FruitColor] = [.Apple(color: "красное"), .Banana(color: "желтый"), .Watermelon(color: "зеленый"), .Peach(color: "розовый")]
    for i in fruits {
        switch i {
        case .Apple(let color):
            print("Яблоко \(color)")
        case .Banana(let color):
            print("Банан \(color)")
        case .Watermelon(let color):
            print("Арбуз (хоть в русском и ягода) \(color)")
        case .Peach(let color):
            print("Персик \(color)")
        }
    }
}

printFruitColors()

//--------------------------------
print("--------------------------------")
// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки

enum Score: String {
  case Five = "Отлично"
  case Four = "Хорошо"
  case Three = "Удовлетворительно"
  case Two = "Неудовлетворительно"
  case One = "Отчисление"
}

func getScore(_ score: Score) -> Int {
  switch score{
    case .Five:
      return 5
    case .Four:
      return 4
    case .Three:
      return 3
    case .Two:
      return 2
    case .One:
      return 1
  }
}

let eval = getScore(Score.Five)
print("Оценка: \(eval)")

//--------------------------------
print("--------------------------------")
// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum

enum Car : String {
  case Audi = "Ауди"
  case Bugatti = "Бугатти"
  case Ford = "Форд"
  case Jaguar = "Ягуар"
}
let garage : [Car] = [.Audi,.Bugatti,.Ford,.Jaguar]

func printCars() {
  print("В гараже \(garage.count) машины:")
  for i in garage {
    print("\(i) (\(i.rawValue))")
  }
}

printCars()




