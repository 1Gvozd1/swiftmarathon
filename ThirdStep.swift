func sortArray(_ array: [Int], by f: (Int, Int) -> Bool) -> [Int] {
    var sorted = array
    for i in 0..<sorted.count {
        for j in i+1..<sorted.count {
            if f(sorted[i], sorted[j]) {
                let perm = sorted[i]
                sorted[i] = sorted[j]
                sorted[j] = perm
            }
        }
    }
    return sorted
}
//Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную
let array = [6, 4, 2, 8, 5, 7, 1, 3, 9]
print("Изначальный массив:", array) 
// Сортировка массива по возрастанию
let sortArrayResultInc = sortArray(array, by: {$0 > $1})
print("Сортировка массива по возрастанию:", sortArrayResultInc) 
// Сортировка массива по убыванию
let sortArrayResultDec = sortArray(array, by: {$0 < $1})
print("Сортировка массива по убыванию:", sortArrayResultDec) 

//--------------------------------------------
print("-------------------------") 


//Создать метод, который принимает имена друзей, после этого имена положить в массив
func addFriends(_ names: String...) -> [String] {
    var friends = [String]()
    for i in names {
      friends.append(i)
    }
    return friends
}

var friendsArray = addFriends("Александр",  "Игорь", "Максим", "Алексей", "Миша")
print("Изначальный массив:", friendsArray)

//Массив отсортировать по количеству букв в имени
friendsArray.sort {$0 < $1}
print("Отсортированный массив:", friendsArray)

//Создать словарь (Dictionary), где ключ - кол-во символов в имени, а в значении - имя друга
var friendsDict = [Int:String]()
for i in friendsArray {

  friendsDict[i.count] = i
}
print("Cловарь:",friendsDict)

//Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение
func showKeyAndValue(_ key: Int) {
  if let value = friendsDict[key] {
        print("Ключ: \(key) Значение: \(value)")
    } else {
        print("Ключ \(key) не найден")
    }
}
showKeyAndValue(4)
showKeyAndValue(10)

//--------------------------------------------
print("-------------------------") 
//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль
func checkEmptyArrays(_ stringArray: [String], _ numArray: [Int]) {
    var stringArray = stringArray
    var numArray = numArray
    
    if stringArray.isEmpty {
        stringArray.append("Строка по умолчанию")
    }
    
    if numArray.isEmpty {
        numArray.append(0)
    }
    
    print("Массив строк: \(stringArray)")
    print("Массив чисел: \(numArray)")
}
let emptyStringArray: [String] = []
let emptyNumArray: [Int] = []
checkEmptyArrays(emptyStringArray, emptyNumArray)

let noEmptyStringArray: [String] = ["Первый","Второй","Третий"]
let noEmptyNumArray: [Int] = [1,2,3]
checkEmptyArrays(noEmptyStringArray, noEmptyNumArray)




