let daysArray = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

let monthArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

print("---------------------------")
for i in daysArray {
    print("\(i) days")
}
print("---------------------------")
for i in 0 ..< monthArray.count {
    print("\(monthArray[i]) has \(daysArray[i]) days")
}
print("---------------------------")
var tupleArray = [(month: String, days: Int)]()
for i in 0..<monthArray.count {
    tupleArray.append((monthArray[i], daysArray[i]))
}
for (month, days) in tupleArray {
    print("\(month) has \(days) days")
}
print("---------------------------")
 
var reverse = monthArray.count - 1
 
for i in 0..<monthArray.count {
    print("\(monthArray[(i + reverse)]) has \(daysArray[i + reverse]) days")
    reverse -= 2
}
print("---------------------------")

let date = (month:5, days:2)
var daysResult = 0
for i in 0..<(date.month) {
  daysResult += daysArray[i]
}
print("From the beginning of the year to \(date.days) \(monthArray[date.month]) \(daysResult+date.days) days")